package Modulo1.AgilitySubMod;

import java.util.Arrays;
import java.util.Scanner;

public class Agility
{
    //bigger than

    //Show if the first number is bigger than the second
    public static boolean biggerThan(String numA, String numB)
    {
        boolean Result;
        double NumA,NumB;

        NumA = Double.parseDouble(numA);
        NumB = Double.parseDouble(numB);

        Result=false;

        if (NumA > NumB)
        {
            Result = true;
        }

        return Result;
        //solved code
    }

    //Sort from bigger the numbers an show in array
    public static int[] order(int numA, int numB, int numC,int numD,int numE)
    {
        int[] numbers = new int[]{numA, numB, numC, numD, numE};

        Arrays.sort(numbers);

        return numbers;
        //solved code
    }

    //Look for the smaller number of the array
    public static double smallerThan(double array [])
    {
        double min;
        min = array[0];

        for(int x = 0; x < array.length; x++)
        {
            if(array[x] < min)
            {
                min = array[x];
            }
        }

        return min;
        //solved code
    }

    //Palindrome number is called in Spanish capicúa
    //The number is palindrome
    public static boolean palindromeNumber(int numA)
    {
        int remain,sum,numB;
        boolean result;

        sum = 0;
        result = false;
        numB = numA;

        while(numA > 0)
        {
            remain = numA % 10;
            sum = (sum*10) + remain;
            numA = numA/10;
        }

        if(numB == sum)
        {
            result = true;
        }

        return result;
        //solved code
    }

    //the word is palindrome
    public static boolean palindromeWord(String word)
    {
        String Rev;
        int charnum;
        boolean result;

        Rev = "";
        charnum = word.length();
        result = false;

        for(int x = charnum-1; x >= 0 ; x-- )
        {
            Rev = Rev + word.charAt(x);
        }

        if(word.equals(Rev))
        {
            result = true;
        }
        //solved code
        return result;
    }

    //Show the factorial number for the parameter
    public static int factorial(int numA)
    {
        int fact = 1;

        if(numA == 0)
        {
            fact = 1;
        }
        else
        {
            for(int x = 1; x <= numA; x++)
            {
                fact = fact*x;
            }
        }
        return fact;
        //solved code
    }

    //is the number odd
    public static boolean isOdd(byte numA)
    {
        boolean result;
        result = true;

        if ((numA % 2) == 0)
        {
            result = false;
        }

        return result;
        //solved code
    }

    //is the number prime
    public static boolean isPrimeNumber(int numA)
    {
        boolean result;
        int MOD_num;

        MOD_num = 0;
        result = false;

        //this code validates how many divisors does the number have
        for (int x = 1; x <= numA; x++)
        {
            if((numA % x) == 0)
            {
                MOD_num = MOD_num + 1;
            }
        }

        //if the number have 2 or less divisors means the number is prime
        if (MOD_num == 2)
        {
            result = true;
        }
        return result;
        //solved code
    }

    //is the number even
    public static boolean isEven(byte numA)
    {
        boolean result;

        result = false;

        if((numA % 2) == 0)
        {
            result = true;
        }

        return result;
        //solved code
    }

    //is the number perfect
    public static boolean isPerfectNumber(int numA)
    {
        boolean result;
        int sum;

        sum = 0;
        result = false;

        for(int x =1; x < numA; x++)
        {
            //This section defines the divisors of the number and sum them
            if((numA % x) == 0)
            {
                sum = sum + x;
            }

            if(sum == numA)
            {
                result = true;
            }
        }

        return result;
        //solved code
    }

    //Return an array with the fibonacci sequence for the requested number
    public static int [] fibonacci(int numA)
    {
        int[] secuence;

        secuence = new int [numA+1];

        secuence[0] = 0;
        secuence[1] = 1;
        if(numA >= 2)
        {
            for (int x = 2; x < secuence.length;x++)
            {
                secuence[x] = secuence[x - 1] + secuence[x - 2];
            }
        }
        return secuence;
        //solved code
    }

    //how many times the number is divided by 3
    public static int timesDividedByThree(int numA)
    {
        int times;

        times = 0;

        for(int x = 1; x <= numA; x++)
        {
            if((x % 3) == 0)
            {
                times = times +  1;
            }
        }
        return times;
    }

    //The game of fizzbuzz
    public static String fizzBuzz(int numA)
    /**
     * If number is divided by 3, show fizz
     * If number is divided by 5, show buzz
     * If number is divided by 3 and 5, show fizzbuzz
     * in other cases, show the number
     */
    {
        String word;

        word = Integer.toString(numA);

        if( ((numA % 3) == 0) && ((numA % 5) == 0) )
        {
            word = "FizzBuzz";
        }
        else if((numA % 3) == 0)
        {
            word = "Fizz";
        }
        else if((numA % 5) == 0)
        {
            word = "Buzz";
        }

        return word;
        //solved code
    }
}
