package Modulo1.AgilitySubMod;

import Modulo1.MenuMod1;

import java.util.Arrays;
import java.util.Scanner;

public class MenuAgi
{
    private Scanner read = new Scanner(System.in);
    private int choice;
    private boolean Status = true;//Enable-Disable Loop
    public MenuAgi()
    {
        while (Status == true)
        {
            dataIntegration();
        }
    }
    private void dataIntegration()
    {
        options();
        cases(getChoice());
    }

    public static void options()
    {
        System.out.println("Seleccione una función");
        System.out.print("1.BiggerThan        ");
        System.out.println("2.Order");
        System.out.print("3.SmallerThan       ");
        System.out.println("4.palindromeNumber");
        System.out.print("5.PalindromeWord    ");
        System.out.println("6.Factorial");
        System.out.print("7.isOdd             ");
        System.out.println("8.isPrimeNumber");
        System.out.print("9.isEven            ");
        System.out.println("10.isPerfectNumber");
        System.out.print("11.Fibonacci        ");
        System.out.println("12.timesDividedBy3");
        System.out.print("13.FizzBuzz         ");
        System.out.println("14.Back");
    }

    public int getChoice()
    {
        choice = read.nextInt();
        return choice;
    }

    //Método para acceder a las opciones
    public void cases(int Choice)
    {
        int z,x,y,w,v;
        String a,b;
        byte Num;
        double[] Valores;
        int[] res2;
        boolean res;
        double res3;
        switch (Choice)
        {
            case 1:
                System.out.println("Ingrese el valor 1");
                read.nextLine();
                a = read.nextLine();
                System.out.println("Ingrese el valor 2");
                b = read.nextLine();
                res = Agility.biggerThan(a,b);
                if(res == true )
                {
                    System.out.println(a+" es mayor que "+b);
                }
                else
                {
                    System.out.println(a+" es menor que "+b);
                }
                System.out.println("Presione intro para regresar");
                read.nextLine();
                break;
            case 2:
                System.out.println("Ingrese el valor 1");
                z = read.nextInt();
                System.out.println("Ingrese el valor 2");
                x = read.nextInt();
                System.out.println("Ingrese el valor 3");
                y = read.nextInt();
                System.out.println("Ingrese el valor 4");
                w = read.nextInt();
                System.out.println("Ingrese el valor 5");
                v = read.nextInt();
                res2 = Agility.order(z,x,y,w,v);
                System.out.println("Orden: "+ Arrays.toString(res2));
                System.out.println("Presione intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 3:
                System.out.println("Cantidad de nmeros a procesar");
                x=read.nextInt();
                Valores = new double[x];
                for(y=0;y < Valores.length; y++)
                {
                    System.out.println("Ingrese el valor " +(y+1));
                    Valores[y] = read.nextInt();
                }
                res3 = Agility.smallerThan(Valores);
                System.out.println("El número menor es: "+ res3);
                System.out.println("Presione intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 4:
                System.out.println("Ingrese el número");
                x=read.nextInt();
                res = Agility.palindromeNumber(x);
                if(res == true)
                {
                    System.out.println("El valor es palindromo");
                }
                else
                {
                    System.out.println("El valor no es palindromo");
                }
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 5:
                System.out.println("Ingrese la palabra");
                read.nextLine();
                a=read.nextLine();
                res = Agility.palindromeWord(a);
                if(res == true)
                {
                    System.out.println("El valor es palindromo");
                }
                else
                {
                    System.out.println("El valor no es palindromo");
                }
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                break;
            case 6:
                System.out.println("Ingrese el número");
                x=read.nextInt();
                y = Agility.factorial(x);
                System.out.println("El factorial de "+ x + " es "+ y);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 7:
                System.out.println("Ingrese el número");
                Num=read.nextByte();
                res = Agility.isOdd(Num);
                if(res == false)
                {
                    System.out.println("El valor es par");
                }
                else
                {
                    System.out.println("El valor es impar");
                }
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 8:
                System.out.println("Ingrese el número");
                x= read.nextInt();
                res = Agility.isPrimeNumber(x);
                if(res == true)
                {
                    System.out.println("El valor es primo");
                }
                else
                {
                    System.out.println("El valor no es primo");
                }
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 9:
                System.out.println("Ingrese el número");
                Num = read.nextByte();
                res = Agility.isEven(Num);
                if(res == true)
                {
                    System.out.println("El valor es par");
                }
                else
                {
                    System.out.println("El valor es impar");
                }
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 10:
                System.out.println("Ingrese el valor");
                x=read.nextInt();
                res = Agility.isPerfectNumber(x);
                if(res == true)
                {
                    System.out.println("El valor es un número perfecto");
                }
                else
                {
                    System.out.println("El valor no es un número perfecto");
                }
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 11:
                System.out.println("Ingrese el valor");
                x=read.nextInt();
                res2 = Agility.fibonacci(x);
                System.out.println("La serie de fibonacci es: "+ Arrays.toString(res2));
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 12:
                System.out.println("Ingrese el valor");
                x=read.nextInt();
                y = Agility.timesDividedByThree(x);
                System.out.println("Las veces en que " +x+" se puede divir en 3 son "+y);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 13:
                System.out.println("Ingrese el valor");
                x=read.nextInt();
                a= Agility.fizzBuzz(x);
                System.out.println(a);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 14:
                MenuMod1 M = new MenuMod1();
                Status = false;
                break;
            default:
                System.out.println("Ingrese un valor válido");
                break;
        }
    }
}