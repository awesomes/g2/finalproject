package Modulo1.GeometrySubMod;

public class Geometry
{
    //Calculate the area for a square using one side
    public static int squareArea(int side)
    {
        int result;

        result = 0;

        if(side > 0)
        {
            result = side * side;
        }
        else if(side <= 0)
        {
            throw new IllegalArgumentException();
        }

        return result;
        //solved code
    }

    //Calculate the area for a square using two sides
    public static int squareArea(int sidea, int sideb)
    {
        int result;
        if(sidea > 0 && sideb > 0)
        {
            result = sidea * sideb;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return result;
        //solved code
    }

    //Calculate the area of circle with the radius
    public static double circleArea(double radius)
    {
        double result;
        if(radius >= 0 )
        {
            result = (radius*radius)*3.1416;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return result;
    }

    //Calculate the perimeter of circle with the diameter
    public static double circlePerimeter(int diameter)
    {
        double result;
        if(diameter >= 0 )
        {
            result = diameter*3.1416;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return result;
    }

    //Calculate the perimeter of square with a side
    public static double squarePerimeter(double side)
    {
        double result;
        if(side >= 0 )
        {
            result = 4*side;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return result;
    }

    //Calculate the volume of the sphere with the radius
    public static double sphereVolume(double radius)
    {
        double result;
        if(radius >= 0 )
        {
            double r3;
            float div;

            //define div to set a precise value of 3/4
            r3= radius*radius*radius;
            div = (float) 4/3;

            result = (div)*(3.1416*r3);
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return result;
    }

    //Calculate the area of regular pentagon with one side
    public static float pentagonArea(int side)
    {
        float result,half_side,Area_10;
        double h,angle,rad_angle;

        angle= 36;
        Area_10 = 0;

        //this section finds the high of the pentagon
        rad_angle= Math.toRadians(angle);
        half_side = (float) side/2;
        h= (half_side)/Math.tan(rad_angle);

        if(side >= 0 )
        {
            result =  (float) ( ( (half_side*h)/2 )*10 );
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return result;
    }

    //Calculate the Hypotenuse with two cathetus
    public static double calculateHypotenuse(double catA, double catB)
    {
        double result;
        double catA2,catB2;
        if(catA >= 0 && catB >= 0)
        {
            catA2 = catA*catA;
            catB2 = catB* catB;

            result = Math.sqrt(catA2+catB2);
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return result;
    }
}
