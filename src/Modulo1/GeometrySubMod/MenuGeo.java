package Modulo1.GeometrySubMod;

import Modulo1.MenuMod1;

import java.util.Arrays;
import java.util.Scanner;

public class MenuGeo
{
    private Scanner read = new Scanner(System.in);
    private int choice;
    private boolean Status = true;//Enable-Disable Loop
    public MenuGeo()
    {
        while (Status == true)
        {
            dataIntegration();
        }
    }
    private void dataIntegration()
    {
        options();
        cases(getChoice());
    }

    public static void options()
    {
        System.out.println("Seleccione una función");
        System.out.print("1.squareArea           ");
        System.out.println("2.squareArea ");
        System.out.print("3.circleArea            ");
        System.out.println("4.circlePerimeter");
        System.out.print("5.squarePerimeter       ");
        System.out.println("6.sphereVolume");
        System.out.print("7.pentagonArea          ");
        System.out.println("8.calculateHypotenuse");
        System.out.println("9.Back");
    }

    public int getChoice()
    {
        choice = read.nextInt();
        return choice;
    }

    //Método para acceder a las opciones
    public void cases(int Choice)
    {
        int z,x,y;
        float a;
        double res3,q,v;
        switch (Choice)
        {
            case 1:
                System.out.println("Ingrese el valor");
                x = read.nextInt();
                z = Geometry.squareArea(x);
                System.out.println("El área de cuadrado es: "+z);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 2:
                System.out.println("Ingrese el valor1");
                x = read.nextInt();
                System.out.println("Ingrese el valor2");
                y = read.nextInt();
                z = Geometry.squareArea(x,y);
                System.out.println("El área del cuadrado es: "+z);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 3:
                System.out.println("Ingrese el valor");
                q = read.nextDouble();
                res3 = Geometry.circleArea(q);
                System.out.println("El área del circulo es: "+res3);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 4:
                System.out.println("Ingrese el valor");
                x = read.nextInt();
                res3 = Geometry.circlePerimeter(x);
                System.out.println("El perimetro del circulo es es: "+res3);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 5:
                System.out.println("Ingrese el valor");
                q = read.nextDouble();
                res3 = Geometry.squarePerimeter(q);
                System.out.println("El Perimetro de cuadrado es: "+res3);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 6:
                System.out.println("Ingrese el valor");
                q = read.nextDouble();
                res3 = Geometry.sphereVolume(q);
                System.out.println("El volumen de la esfera es: "+res3);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 7:
                System.out.println("Ingrese el valor");
                x = read.nextInt();
                a = Geometry.pentagonArea(x);
                System.out.println("El Área del pentagono es: "+a);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 8:
                System.out.println("Ingrese el valor 1");
                q = read.nextDouble();
                System.out.println("Ingrese el valor 2");
                v = read.nextDouble();
                res3 = Geometry.calculateHypotenuse(q,v);
                System.out.println("El La hipotenusa es: "+res3);
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 9:
                MenuMod1 M = new MenuMod1();
                Status = false;
                break;
            default:
                System.out.println("Ingrese un valor válido");
                break;
        }
    }
}
