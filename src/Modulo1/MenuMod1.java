package Modulo1;

import Modulo1.AgilitySubMod.MenuAgi;
import Modulo1.ConvertionSubMod.MenuConv;
import Modulo1.GeometrySubMod.MenuGeo;
import com.sun.tools.javac.Main;

import java.util.Scanner;

public class MenuMod1
{
    private int x;
    boolean valida = true;
    Scanner leer = new Scanner(System.in);

    public MenuMod1()
    {

        loop();
    }

    private static void info()
    {
        System.out.println("Seleccione el método a utilizar");
        System.out.println("1.Agility");
        System.out.println("2.Convertion");
        System.out.println("3.Geometric");
        System.out.println("4.Volver");
    }

    private int catchInfo()
    {
        x = leer.nextInt();

        return x;
    }

    private void choice(int a)
    {
        switch (a)
        {
            case 1:
                MenuAgi A = new MenuAgi();
                break;
            case 2:
                MenuConv C = new MenuConv();
                break;
            case 3:
                MenuGeo G = new MenuGeo();
                break;
            case 4:
                Main M = new Main();
                valida = false;
                break;
            default:
                System.out.println("Ingrese un valor valido");
                break;
        }
    }

    private void loop()
    {
        while  (valida == true)
        {
            info();
            choice(catchInfo());
        }
    }
}
