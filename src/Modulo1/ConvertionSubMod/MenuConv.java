package Modulo1.ConvertionSubMod;

import Modulo1.MenuMod1;

import java.util.Scanner;

public class MenuConv
{
    private Scanner read = new Scanner(System.in);
    private int choice;
    private boolean Status = true;//Enable-Disable Loop
    public MenuConv()
    {
        while (Status == true)
        {
            dataIntegration();
        }
    }
    private void dataIntegration()
    {
        options();
        cases(getChoice());
    }

    public static void options()
    {
        System.out.println("Seleccione una función");
        System.out.print("1.kmToM1                  ");
        System.out.println("2.kmTom");
        System.out.print("3.kmTocm                  ");
        System.out.println("4.mmTom");
        System.out.print("5.milesToFoot             ");
        System.out.println("6.yardToInch");
        System.out.print("7.inchToMiles             ");
        System.out.println("8.footToYard");
        System.out.print("9.kmToInch                ");
        System.out.println("10.mmToFoot");
        System.out.print("11.yardToCm               ");
        System.out.println("13.Back");
    }

    public int getChoice()
    {
        choice = read.nextInt();
        return choice;
    }

    //Método para acceder a las opciones
    public void cases(int Choice)
    {
        int z,x,y;
        float a;
        String f;
        double res3,q,v;
        switch (Choice)
        {
            case 1:
                System.out.println("Ingrese el valor");
                q = read.nextDouble();
                z = Convertion.kmToM1(q);
                System.out.println(q+" Kilometros equivalen a "+z+" Metros");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 2:
                System.out.println("Ingrese el valor");
                q = read.nextDouble();
                res3 = Convertion.kmTom(q);
                System.out.println(q+" Kilometros equivalen a "+res3+" Metros");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 3:
                System.out.println("Ingrese el valor");
                q = read.nextDouble();
                res3 = Convertion.kmTocm(q);
                System.out.println(q+" Kilometros equivalen a "+res3+" Centimetros");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 4:
                System.out.println("Ingrese el valor");
                x = read.nextInt();
                res3 = Convertion.mmTom(x);
                System.out.println(x+" Milimetros equivalen a "+res3+" metros");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 5:
                System.out.println("Ingrese el valor");
                q = read.nextDouble();
                res3 = Convertion.milesToFoot(q);
                System.out.println(q+" Millas equivalen a "+res3+" Pies");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 6:
                System.out.println("Ingrese el valor");
                x = read.nextInt();
                y = Convertion.yardToInch(x);
                System.out.println(x+" Yardas equivalen a "+y+" Pulgadas");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 7:
                System.out.println("Ingrese el valor");
                q = read.nextInt();
                res3 = Convertion.inchToMiles(q);
                System.out.println(q+" pulgadas equivalen a "+res3+" Millas");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 8:
                System.out.println("Ingrese el valor");
                x = read.nextInt();
                y = Convertion.footToYard(x);
                System.out.println(x+" pies equivalen a "+y+" yardas");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                read.nextLine();
                break;
            case 9:
                System.out.println("Ingrese el valor");
                read.nextLine();
                f = read.nextLine();
                res3 = Convertion.kmToInch(f);
                System.out.println(f+" pies equivalen a "+res3+" yardas");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                break;
            case 10:
                System.out.println("Ingrese el valor");
                read.nextLine();
                f = read.nextLine();
                res3 = Convertion.mmToFoot(f);
                System.out.println(f+" milimetrs equivalen a "+res3+" pies");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                break;
            case 11:
                System.out.println("Ingrese el valor");
                read.nextLine();
                f = read.nextLine();
                res3 = Convertion.yardToCm(f);
                System.out.println(f+" yardas equivalen a "+res3+" centimetros");
                System.out.println("Presione Intro para continuar");
                read.nextLine();
                break;
            case 13:
                MenuMod1 M = new MenuMod1();
                Status = false;
                break;
            default:
                System.out.println("Ingrese un valor válido");
                break;
        }
    }
}
