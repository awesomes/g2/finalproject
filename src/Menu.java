import Modulo1.MenuMod1;
import Modulo2.MenuMod2;
import Modulo3.MenuPrincipal;
import Modulo4.MenuMod4;



import java.util.Scanner;

public class Menu
{
    private Scanner read = new Scanner(System.in);
    private int choice;
    private boolean Status = true;//Enable-Disable Loop

    public Menu()
    {
        while (Status == true)
        {
            dataIntegration();
        }
    }

    private void dataIntegration()
    {
        options();
        cases(getChoice());
    }

    public static void options()
    {
        System.out.println("Ingrese un número según lo que requiera");
        System.out.println("1.Utilidades");
        System.out.println("2.Simulador lotería");
        System.out.println("3.Simulador de parqueadero");
        System.out.println("4.Simulador de TV");
        System.out.println("5.Salir");
    }

    public int getChoice()
    {
        choice = read.nextInt();

        return choice;
    }

    //Método para acceder a las opciones
    public void cases(int Choice)
    {
        switch (Choice)
        {
            case 1:
                MenuMod1 m = new MenuMod1();
                break;
            case 2:
                MenuMod2 m2 = new MenuMod2();
                break;
            case 3:
                MenuPrincipal m3 = new MenuPrincipal();
                break;
            case 4:
                MenuMod4 m4 =new MenuMod4();
                break;
            case 5:
                Status = false;
                break;
            default:
                System.out.println("Ingrese un valor válido");
                break;
        }
    }
}




